@extends('master.master')

@section('title')
    Detail Cast
@endsection

@section('content')
    
<h1>{{$cast->nama}}</h1>
<h4>Umur : {{$cast->umur}}</h4>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-danger btn-sm">Kembali</a>

@endsection