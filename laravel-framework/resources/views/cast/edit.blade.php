@extends('master.master')

@section('title')
    Edit Cast
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Nama" value="{{$cast->nama}}">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="number" class="form-control" name="umur" placeholder="Umur" min="0" value="{{$cast->umur}}">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label> <br>
        <textarea name="bio" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Edit</button>
</form>

@endsection