@extends('master.master')

@section('title')
    Cast
@endsection

@push('scripts')
  <script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#tableCast").DataTable();
    });
  </script>
@endpush

@push('styles')
  <link rel="stylesheet" href="{{asset('/template/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
@endpush


@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah</a>

<table class="table" id="tableCast">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
      {{-- <th scope="col">Umur</th>
      <th scope="col">Bio</th> --}}
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                    <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                </form>
            </td>
            {{-- <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td> --}}
        </tr>
    @empty
        <p>No users</p>
    @endforelse
  </tbody>
</table>

@endsection