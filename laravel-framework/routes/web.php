<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [AuthController::class, 'welcome']);


Route::get('/master', function() {
    return view('master.master');
});

Route::get('/table', function () {
    return view('pages.table');
});

Route::get('/data-table', function () {
    return view('pages.data_table');
});


//CRUD Cast

//Create
//Form Tambah Cast
Route::get('/cast/create', [CastController::class, 'create']);
//Tambah data ke Database
Route::post('/cast', [CastController::class, 'store']);

//Read
//Tampil semua data pada database
Route::get('/cast', [CastController::class, 'index']);
//Detail Cast berdasarkan ID
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update
//Form Edit Cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//Update data ke Database
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete
//Delete Cast dari ID
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);