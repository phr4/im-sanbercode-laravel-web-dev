<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP PHP</title>
</head>
<body>
    <?php 
        // Require Files
        require_once "animal.php";
        require_once "ape.php";
        require_once "frog.php";
        
        // Intansiasi
        $sheep = new Animal("shaun");
        $sungokong = new Ape("kera sakti");
        $kodok = new Frog("buduk");
    
        echo "Name : ". $sheep->name ."<br>"; // "shaun"
        echo "legs : ". $sheep->legs ."<br>"; // 4
        echo "cold blooded : ". $sheep->cold_blooded ."<br><br>"; // "no"

        echo "Name : ". $kodok->name . "<br>"; // "buduk"
        echo "legs : ". $kodok->legs . "<br>"; // 4
        echo "cold blooded : ". $kodok->cold_blooded . "<br>"; // "no"
        echo "Jump : " .$kodok->jump(). "<br><br>"; // "hop hop"

        echo "Name : ". $sungokong->name . "<br>"; // "kera sakti"
        echo "legs : ". $sungokong->legs . "<br>"; // 2
        echo "cold blooded : ". $sungokong->cold_blooded . "<br>"; // "no"
        echo "Yell : ". $sungokong->yell(). "<br><br>" ; // "Auooo"

    ?>
</body>
</html>